Source: sctk
Maintainer: Giulio Paci <giuliopaci@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper,
               debhelper-compat(= 13),
               librsvg2-bin,
               texlive-latex-base,
               texlive-latex-extra,
               poppler-utils,
               imagemagick,
               ghostscript,
               gnuplot-nox | gnuplot
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/sctk
Vcs-Git: https://salsa.debian.org/debian/sctk.git
Homepage: https://www.nist.gov/itl/iad/mig/tools.cfm
Rules-Requires-Root: binary-targets

Package: sctk
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: libsgmls-perl
Description: speech recognition scoring toolkit
 This software can be used to test quality performance of
 speech recognition software.
 .
 It includes the SCLITE, ASCLITE, tranfilt, hubscr and utf_filt
 scoring tools.

Package: sctk-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: speech recognition scoring toolkit (documentation)
 This software can be used to test quality performance of
 speech recognition software.
 .
 It includes the SCLITE, ASCLITE, tranfilt, hubscr and utf_filt
 scoring tools.
 .
 This package provides the documentation.
